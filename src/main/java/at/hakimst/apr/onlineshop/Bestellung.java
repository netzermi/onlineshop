package at.hakimst.apr.onlineshop;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class Bestellung {
    private String nummer;
    private Date datum;
    private Kunde kunde;
    private ArrayList<Bestellposition> bestellpositionen;

    private boolean bezahlt;

    private boolean geliefert;

    public Bestellung(Kunde kunde) {
        this.nummer = UUID.randomUUID().toString();
        datum = new Date();
        this.kunde = kunde;
        this.bestellpositionen = new ArrayList<>();
        this.bezahlt = false;
        this.geliefert = false;
    }

    public String getNummer() {
        return nummer;
    }

    public Date getDatum() {
        return datum;
    }

    public Kunde getKunde() {
        return kunde;
    }

    public void setStatusGeliefert(boolean lieferstatus)
    {
        this.geliefert = lieferstatus;
    }

    public void setStatusBezahlt(boolean bezahlstatus)
    {
        this.bezahlt = bezahlstatus;
    }

    public ArrayList<Bestellposition> getBestellpositionen() {
        return bestellpositionen;
    }

    @Override
    public String toString() {
        return "Bestellung{" +
                "nummer='" + nummer + '\'' +
                ", datum=" + datum +
                ", kunde=" + kunde +
                ", bestellpositionen=" + bestellpositionen +
                ", bezahlt=" + bezahlt +
                ", geliefert=" + geliefert +
                '}';
    }

    public boolean istBezahlt() {
        return bezahlt;
    }

    /*
    mögliche METHODEN
    - addBestellposition() Bestellposition zur Bestellung hinzufügen
    - bestellpositionEntfernen() Bestellposition per Positionsnummer aus der Liste entfernen
    - gesamtNettoBetrag() der Bestellung zurückgeben als double
    - gesamtBruttoBetrag() der Bestellung zurückgeben als double
    - bestellungDrucken() alle Bestelldetails incl. Nummer, Datum, Kunde, allen Bestellposition, Zeilenweise Netto und Brutto Gesamt, Netto und Brutto für die Bestellung gesamt
     */
}
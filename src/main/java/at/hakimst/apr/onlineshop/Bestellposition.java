package at.hakimst.apr.onlineshop;

public class Bestellposition {
    private Artikel artikel;
    private int menge;

    public Bestellposition(Artikel artikel, int menge) {
        this.artikel = artikel;
        this.menge = menge;
    }

    public Artikel getArtikel() {
        return artikel;
    }

    public int getMenge() {
        return menge;
    }

    public double nettoGesamt(){
        return  artikel.getNettopreis() * menge;
    }
    /*
    Mögliche Methoden
    - nettoGesamt() je Bestellposition
    - bruttogesamt() je Bestellposition
     */
}

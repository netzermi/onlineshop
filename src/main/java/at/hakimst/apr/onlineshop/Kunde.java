package at.hakimst.apr.onlineshop;

public class Kunde {
    private String nummer;
    private String name;
    private String straße;
    private String plz;
    private String ort;

    public Kunde(String nummer, String name, String straße, String plz, String ort) {
        this.nummer = nummer;
        this.name = name;
        this.straße = straße;
        this.plz = plz;
        this.ort = ort;
    }

    public String getNummer() {
        return nummer;
    }

    public String getName() {
        return name;
    }

    public String getStraße() {
        return straße;
    }

    public String getPlz() {
        return plz;
    }

    public String getOrt() {
        return ort;
    }

    @Override
    public String toString() {
        return "Kunde{" +
                "nummer='" + nummer + '\'' +
                ", name='" + name + '\'' +
                ", straße='" + straße + '\'' +
                ", plz='" + plz + '\'' +
                ", ort='" + ort + '\'' +
                '}';
    }
}
package at.hakimst.apr.onlineshop;

import java.util.ArrayList;

public class Kundendatenbank {
    private ArrayList<Kunde> kundenListe;

    public Kundendatenbank(){
        kundenListe = new ArrayList<>();
    }

    public void kundeHinzufügen(Kunde k){
        kundenListe.add(k);
    }

    public Kunde kundeSuchen(String nummer){
        for(Kunde k : kundenListe){
            if(k.getNummer().equals(nummer)) {
                return k;
            }
        }
        return null;
    }















    /*
    Die Kundendatenbank besteht aus einer Liste von Kunden.

    mögliche Methoden:
    - Kunde der Liste hinzufügen
    - Kunde per Kundennummer suchen und zurückgeben
    - Kunde per Kundennummer aus der Liste entfernen
    - Liste mit allen Kunden zurückgeben
    - Liste mit allen Kunden einer bestimmten PLZ zurückgeben
    - Kunden per Name suchen und eine Liste von Kunden zurückgeben, die den Suchstring im Namen haben
    - Kundendatenbank drucken
    ...
     */
}

package at.hakimst.apr.onlineshop;

public class Artikel {
    private String nummer;
    private String name;
    private String kategorie;
    private double nettopreis;
    private int ustSatz;

    public Artikel(String nummer, String name, String kategorie, double nettopreis, int ustSatz) {
        this.nummer = nummer;
        this.name = name;
        this.kategorie = kategorie;
        this.nettopreis = nettopreis;
        this.ustSatz = ustSatz;
    }

    public String getNummer() {
        return nummer;
    }

    public String getName() {
        return name;
    }

    public String getKategorie() {
        return kategorie;
    }

    public double getNettopreis() {
        return nettopreis;
    }

    public int getUstSatz() {
        return ustSatz;
    }

    @Override
    public String toString() {
        return nummer + " " + name + " " + kategorie + "  " + nettopreis + " " + ustSatz;
    }



    /*
    Mögliche Methoden
    - getBruttopreis() liefert den Bruttopreis der Artikels (berechnet)
     */
}

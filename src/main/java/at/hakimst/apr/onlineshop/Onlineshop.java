package at.hakimst.apr.onlineshop;

import java.util.ArrayList;
import java.util.List;

public class Onlineshop {
    private List<Bestellung> bestellungen;

    public Onlineshop(){
        this.bestellungen = new ArrayList<>();
    }

    // Liste mit allen nicht bezahlten Bestellungen
    public List<Bestellung> unbezahlteBestellungen(){
        List<Bestellung> unbezahlteBestellungen = new ArrayList<>();
        for(Bestellung b : bestellungen){
            //if(b.istBezahlt == false)
            if(!b.istBezahlt()){
                unbezahlteBestellungen.add(b);
            }
        }
        return unbezahlteBestellungen;
    }
    // Mögliche Funktionen eines Onlineshop
    // Der Online-Shop besteht aus einer Liste von Bestellungen
    // Neue Bestellung für bestimmten Kunden hinzufügen
    // Liste mit allen bezahlten aber nicht ausgelieferten Bestellungen
    // Summe der bezahlten Bestellungen
    // Summe aller nicht bezahlten Bestellungen
    // Alle Bestellungen eines bestimmten Kunden (per Kundennummer abgefragt)
    // Alle Bestellungen einer bestimmten PLZ
    // Anzahl der Bestellungen mit einem Artikel einer bestimmten Kategorie


    //...

}

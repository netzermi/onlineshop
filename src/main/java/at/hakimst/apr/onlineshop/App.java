package at.hakimst.apr.onlineshop;

public class App {

    public static void main(String[] args) {
        Artikel a1 = new Artikel("s8", "PS5", "Konsole", 500, 20);
        Artikel a2 = new Artikel("s9", "XBO", "Konsole", 450, 20);
        Artikel a3 = new Artikel("s10", "NSW", "Konsole", 450, 20);
        Artikel a4 = new Artikel("s11", "SD", "Konsole", 280, 20);
        Artikeldatenbank adb = new Artikeldatenbank();
        adb.artikelHinzufuegen(a1);
        adb.artikelHinzufuegen(a2);
        adb.artikelHinzufuegen(a3);
        adb.artikelHinzufuegen(a4);
        adb.artikelLöschen("s8");
        System.out.println(adb.alleArtikel());


    }
}

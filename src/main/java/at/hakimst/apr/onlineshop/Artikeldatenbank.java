package at.hakimst.apr.onlineshop;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Artikeldatenbank {
    private List<Artikel> artikelListe;

    public Artikeldatenbank(){
        artikelListe = new ArrayList<>();
    }

    public void artikelHinzufuegen(Artikel artikel){
        artikelListe.add(artikel);
    }


    public Artikel artikelSuchen(String nummer){
        for(Artikel a : artikelListe){
            if(a.getNummer().equals(nummer)){
                return a;
            }
        }
        return null;
    }

    public boolean artikelLöschen(String nummer){
        Iterator<Artikel> iterator = artikelListe.iterator();
        while (iterator.hasNext()){
            Artikel a = iterator.next();
            if(a.getNummer().equals(nummer)){
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    public void artikelLöschenOhneIter(String nummer){
        for(Artikel a : artikelListe){
            if(a.getNummer().equals(nummer)){
                artikelListe.remove(a);
            }
        }
    }
    public int anzahlArtikel(){
        return artikelListe.size();
    }

    public List<Artikel> alleArtikel(){
        return artikelListe;
    }

    public List<Artikel> artikelEinerKategorie(String kategorie){
        List<Artikel> tmp = new ArrayList<>();
        for(Artikel a : artikelListe){
            if(a.getKategorie().equalsIgnoreCase(kategorie)){
                tmp.add(a);
            }
        }
        return tmp;
    }





    /*
    Die Artikeldatenbank besteht aus einer Liste von Artikeln.

    Mögliche Methoden:

    - Artikel der Liste hinzufügen
    - Artikel per Nummer suchen und zurückgeben (Nummer ist eindeutig)
    - Artikel per Artikelnummer aus der Datenbank entfernen
    - Liste mit allen Artikeln zurückgeben
    - Liste mit allen Artikeln einer bestimmten Kategorie zurückgeben
    - Artikel per Name suchen und eine Liste von Artikeln zurückgeben, die den Suchstring im Namen haben
    - Artikeldatenbank drucken
    - ...

     */

}
